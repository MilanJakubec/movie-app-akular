import ContentLayout from "../layout/ContentLayout";
import MovieCard from "../components/MovieCard";
import React, { useContext, useState } from "react";
import { Movie } from "../utils/types";
import { CircularProgress, Grid, Typography } from "@mui/material";
import FavoritesContext from "../utils/FavoritesContext";

const Favorites = () => {
  const { favorites, addRemoveFavorite } = useContext(FavoritesContext);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const handleIconClick = (movie: Movie): void => {
    setIsLoading(true);
    addRemoveFavorite(movie);
    setIsLoading(false);
  };

  return (
    <ContentLayout
      title="My favorite movies"
      children={
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justifyContent="center"
          style={{ minHeight: "55vh" }}
        >
          {isLoading ? (
            <CircularProgress />
          ) : favorites && favorites.length > 0 ? (
            <MovieCard
              favorites={favorites}
              movies={favorites}
              handleIconClick={handleIconClick}
            />
          ) : (
            <Typography variant="body2">No content found.</Typography>
          )}
        </Grid>
      }
    />
  );
};

export default Favorites;
