import ContentLayout from "../layout/ContentLayout";
import { Link } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";

const NoContent = () => {
  return (
    <ContentLayout
      title="404 Page Not Found"
      children={
        <>
          <Typography variant="h3" paragraph>
            Sorry, page not found!
          </Typography>

          <Typography sx={{ color: "text.secondary" }}>
            Sorry, we couldn’t find the page you’re looking for. Perhaps you’ve
            mistyped the URL? Be sure to check your spelling.
          </Typography>

          <Box
            component="img"
            src="/illustration_404.svg"
            sx={{ height: 260, mx: "auto", my: { xs: 5, sm: 10 } }}
          />

          <Button to="/" size="large" variant="contained" component={Link}>
            Go to Home
          </Button>
        </>
      }
    />
  );
};

export default NoContent;
