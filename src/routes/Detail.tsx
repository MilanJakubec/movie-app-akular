import ContentLayout from "../layout/ContentLayout";
import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import { MovieDetail } from "../utils/types";
import { CardMedia, CircularProgress, Grid, Typography } from "@mui/material";
import Divider from "@mui/material/Divider";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorder from "@mui/icons-material/FavoriteBorder";
import FavoritesContext from "../utils/FavoritesContext";

const Detail = () => {
  const { imdbID } = useParams();
  const { favorites, addRemoveFavorite } = useContext(FavoritesContext);

  const [movieDetail, setMovieDetail] = useState<MovieDetail | null>(null);

  const { isLoading, error, data } = useQuery("searchData", () =>
    fetch(
      `http://omdbapi.com/?apikey=${process.env.REACT_APP_OMDBAPI_KEY}&i=${imdbID}`
    ).then((res) => res.json())
  );

  useEffect(() => {
    if (data) {
      setMovieDetail(data);
    }
  }, [data]);

  const isFavorite = (): boolean => {
    const found = favorites.find((favorite) => favorite.imdbID === imdbID);

    return !!found;
  };

  return (
    <ContentLayout
      title={movieDetail ? movieDetail.Title : ""}
      children={
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justifyContent="center"
          style={{ minHeight: "55vh" }}
        >
          {isLoading ? (
            <CircularProgress />
          ) : error ? (
            <>
              <Typography variant="h5">Error occurred</Typography>
              <Divider />
              <Typography variant="body2">
                An error has occurred while fetching data.
              </Typography>
            </>
          ) : movieDetail ? (
            <Grid container spacing={2} style={{ textAlign: "left" }}>
              <Grid item xs={12} md={4}>
                <CardMedia
                  component="img"
                  height="600"
                  image={movieDetail.Poster}
                  alt={movieDetail.Title}
                />
              </Grid>
              <Grid item xs={12} md={8}>
                <Grid container spacing={2} style={{ marginBottom: "1em" }}>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Plot:
                    </Typography>
                  </Grid>
                  <Grid item xs={10}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Plot}
                    </Typography>
                  </Grid>
                </Grid>
                <Divider style={{ marginBottom: "1em" }} />
                <Typography
                  variant="h6"
                  component="h6"
                  style={{ fontWeight: "bold" }}
                >
                  Basic info
                </Typography>
                <Grid container spacing={2} style={{ marginBottom: "1em" }}>
                  <Grid item xs={2} md={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Type:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Type}
                    </Typography>
                  </Grid>
                  <Grid item xs={2} md={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Year:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Year}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Runtime:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Runtime}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={2} style={{ marginBottom: "1em" }}>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Released:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Released}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Country:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Country}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Language:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Language}
                    </Typography>
                  </Grid>
                </Grid>
                <Divider style={{ marginBottom: "1em" }} />
                <Typography
                  variant="h6"
                  component="h6"
                  style={{ fontWeight: "bold" }}
                >
                  Additional info
                </Typography>
                <Grid container spacing={2} style={{ marginBottom: "1em" }}>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Director:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Director}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Writer:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Writer}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Actors:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Actors}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={2} style={{ marginBottom: "1em" }}>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Production:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Production}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Box Office:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.BoxOffice}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      DVD:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.DVD}
                    </Typography>
                  </Grid>
                </Grid>
                <Divider style={{ marginBottom: "1em" }} />
                <Typography
                  variant="h6"
                  component="h6"
                  style={{ fontWeight: "bold" }}
                >
                  Rating
                </Typography>
                <Grid container spacing={2} style={{ marginBottom: "1em" }}>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Rated:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.Rated}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Imdb Rating:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.imdbRating}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Imdb Votes:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    <Typography variant="body1" component="span">
                      {movieDetail.imdbVotes}
                    </Typography>
                  </Grid>
                </Grid>
                <Divider style={{ marginBottom: "1em" }} />
                <Typography
                  variant="h6"
                  component="h6"
                  style={{ fontWeight: "bold" }}
                >
                  Links
                </Typography>
                <Grid container spacing={2} style={{ marginBottom: "1em" }}>
                  <Grid item xs={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      style={{ fontWeight: "bold" }}
                    >
                      Website:
                    </Typography>
                  </Grid>
                  <Grid item xs={10} md={2}>
                    {movieDetail.Website !== "N/A" ? (
                      <a href={movieDetail.Website}>{movieDetail.Website}</a>
                    ) : (
                      <Typography variant="body1" component="span">
                        {movieDetail.Website}
                      </Typography>
                    )}
                  </Grid>
                </Grid>
                <Divider style={{ marginBottom: "1em" }} />
                <Typography
                  variant="h6"
                  component="h6"
                  style={{ fontWeight: "bold" }}
                >
                  Favorite
                </Typography>
                <Grid container spacing={2} style={{ marginBottom: "1em" }}>
                  <Grid item xs={10} md={2}>
                    <Typography
                      variant="body1"
                      component="span"
                      onClick={() =>
                        addRemoveFavorite({
                          Title: movieDetail.Title,
                          Year: movieDetail.Year,
                          imdbID: movieDetail.imdbID,
                          Type: movieDetail.Type,
                          Poster: movieDetail.Poster,
                        })
                      }
                    >
                      {isFavorite() ? <FavoriteIcon /> : <FavoriteBorder />}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          ) : (
            <Typography variant="body2">No content found.</Typography>
          )}
        </Grid>
      }
    />
  );
};

export default Detail;
