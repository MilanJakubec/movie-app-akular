import React, { useContext, useEffect, useState } from "react";
import ContentLayout from "../layout/ContentLayout";
import { useQuery } from "react-query";
import MovieCard from "../components/MovieCard";
import { Movie } from "../utils/types";
import {
  CircularProgress,
  Grid,
  IconButton,
  TextField,
  Typography,
} from "@mui/material";
import { SearchOutlined } from "@mui/icons-material";
import Divider from "@mui/material/Divider";
import FavoritesContext from "../utils/FavoritesContext";

const Home = () => {
  const { favorites, addRemoveFavorite } = useContext(FavoritesContext);
  const [movieData, setMovieData] = useState<Movie[]>([]);
  const [searchText, setSearchText] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");

  const { isLoading, error, data, refetch } = useQuery(
    "searchData",
    () =>
      fetch(
        `http://omdbapi.com/?apikey=${process.env.REACT_APP_OMDBAPI_KEY}&s=${searchText}`
      ).then((res) => res.json()),
    { enabled: false }
  );

  useEffect(() => {
    if (data && data.Search && data.Search.length > 0) {
      setMovieData(data.Search);
    } else {
      setMovieData([]);
    }

    if (data && data.error) {
      setErrorMessage(data.error);
    }
  }, [data]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setSearchText(event.target.value);
  };

  const handleSearchClick = (): void => {
    refetch();
  };

  return (
    <ContentLayout
      children={
        <>
          <TextField
            id="standard-search"
            label="Search field"
            type="search"
            variant="standard"
            value={searchText}
            onChange={handleChange}
            InputProps={{
              endAdornment: (
                <IconButton onClick={handleSearchClick}>
                  <SearchOutlined />
                </IconButton>
              ),
            }}
            style={{ paddingBottom: 10 }}
          />
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            style={{ minHeight: "55vh" }}
          >
            {isLoading ? (
              <CircularProgress />
            ) : error ? (
              <>
                <Typography variant="h5">Error occurred</Typography>
                <Divider />
                <Typography variant="body2">
                  An error has occurred while fetching data.
                </Typography>
              </>
            ) : movieData && movieData.length > 0 ? (
              <>
                <MovieCard
                  favorites={favorites}
                  movies={movieData}
                  handleIconClick={addRemoveFavorite}
                />
              </>
            ) : errorMessage ? (
              <Typography variant="body2">{errorMessage}</Typography>
            ) : (
              <Typography variant="body2">No content found.</Typography>
            )}
          </Grid>
        </>
      }
    ></ContentLayout>
  );
};

export default Home;
