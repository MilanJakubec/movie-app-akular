import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  IconButton,
  Typography,
} from "@mui/material";
import { Movie } from "../utils/types";
import FavoriteBorder from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import Divider from "@mui/material/Divider";
import CardActionArea from "@mui/material/CardActionArea";
import { Link } from "react-router-dom";

interface Props {
  favorites: Movie[];
  movies: Movie[];
  handleIconClick: (movie: Movie) => void;
}

const MovieCard = ({ favorites, movies, handleIconClick }: Props) => {
  const isFavorite = (movie: Movie): boolean => {
    const found = favorites.find(
      (favorite) => favorite.imdbID === movie.imdbID
    );

    return !!found;
  };

  return (
    <Grid container spacing={4} style={{ justifyContent: "center", margin: 0 }}>
      {movies.map((movie) => (
        <Grid item key={movie.imdbID}>
          <Card sx={{ width: 250, maxWidth: 345 }}>
            <CardActionArea>
              <Link to={`../detail/${movie.imdbID}`}>
                <CardMedia
                  component="img"
                  height="250"
                  image={
                    movie.Poster !== "N/A"
                      ? movie.Poster
                      : "No-Image-Placeholder.png"
                  }
                  alt={movie.Title}
                />
              </Link>
            </CardActionArea>
            <CardHeader title={movie.Title} />
            <Divider variant="middle" />
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                Type: {movie.Type}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                Year: {movie.Year}
              </Typography>
            </CardContent>
            <Divider variant="middle" />
            <CardActions
              disableSpacing
              style={{ alignItems: "center", justifyContent: "center" }}
            >
              <IconButton
                aria-label="add to favorites"
                onClick={() => handleIconClick(movie)}
              >
                {isFavorite(movie) ? <FavoriteIcon /> : <FavoriteBorder />}
              </IconButton>
            </CardActions>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default MovieCard;
