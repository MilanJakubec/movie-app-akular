import { Box, Typography } from "@mui/material";
import { Link } from "react-router-dom";

const Copyright = () => {
  return (
    <Box sx={{ bgcolor: "#", p: 6 }} component="footer">
      <Typography variant="body2" color="text.secondary" align="center">
        {"Copyright © "}
        <Link to="/">Movie App Akular</Link> {new Date().getFullYear()}
        {"."}
      </Typography>
    </Box>
  );
};

export default Copyright;
