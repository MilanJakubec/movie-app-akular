import React, { useState } from "react";
import "./App.css";
import BasicLayout from "./layout/BasicLayout";
import { Outlet } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import theme from "./theme";
import { CssBaseline, ThemeProvider } from "@mui/material";
import {
  getAllMovieLocalStorage,
  insetToLocalStorage,
  removeFromLocalStorage,
} from "./utils/localStorage";
import { Movie } from "./utils/types";
import { FavoritesProvider } from "./utils/FavoritesContext";

const queryClient = new QueryClient();

function App() {
  const [favorites, setFavorites] = useState<Movie[] | []>(
    getAllMovieLocalStorage()
  );

  const addRemoveFavorite = (movie: Movie): void => {
    const exist = favorites.some(
      (favorite) => favorite.imdbID === movie.imdbID
    );

    if (exist) {
      const tmpFavorites = [
        ...favorites.filter((favorite) => favorite.imdbID !== movie.imdbID),
      ];

      setFavorites(tmpFavorites);
      removeFromLocalStorage(tmpFavorites, movie);
    } else {
      const tmpFavorites = [...favorites];

      tmpFavorites.push(movie);
      setFavorites([...tmpFavorites]);

      insetToLocalStorage(tmpFavorites, movie);
    }
  };

  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <FavoritesProvider value={{ favorites, addRemoveFavorite }}>
          <BasicLayout children={<Outlet />} />
        </FavoritesProvider>
      </ThemeProvider>
    </QueryClientProvider>
  );
}

export default App;
