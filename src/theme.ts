import { lightBlue, red } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";

// A custom theme for this app
const theme = createTheme({
  palette: {
    primary: {
      light: lightBlue["100"],
      main: lightBlue.A400,
      dark: lightBlue["700"],
      contrastText: "#fff",
    },
    secondary: {
      main: lightBlue["400"],
    },
    error: {
      main: red.A400,
    },
  },
});

export default theme;
