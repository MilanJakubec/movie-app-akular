import { styled, Typography } from "@mui/material";

interface Props {
  title?: string;
  children: JSX.Element;
}

const ContentStyle = styled("div")(({ theme }) => ({
  maxWidth: 1920,
  margin: "auto",
  display: "flex",
  justifyContent: "center",
  flexDirection: "column",
  padding: theme.spacing(2, 0),
}));

const ContentLayout = ({ title, children }: Props) => {
  return (
    <>
      <Typography variant="h4" color="inherit" noWrap>
        {title}
      </Typography>
      <ContentStyle sx={{ textAlign: "center", alignItems: "center" }}>
        {children}
      </ContentStyle>
    </>
  );
};

export default ContentLayout;
