import { AppBar, Toolbar, Typography } from "@mui/material";
import Copyright from "../components/Copyright";
import Container from "@mui/material/Container";
import ContentLayout from "./ContentLayout";
import CameraIcon from "@mui/icons-material/PhotoCamera";
import { Link } from "react-router-dom";

interface Props {
  children: JSX.Element;
}

const BasicLayout = ({ children }: Props) => {
  return (
    <>
      <AppBar position="static">
        <Toolbar sx={{ flexWrap: "wrap" }}>
          <CameraIcon sx={{ mr: 2 }} />
          <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
            <Link style={{ color: "#fff", textDecoration: "none" }} to={"/"}>
              Movie App Akular
            </Link>
          </Typography>
          <nav>
            <Link
              style={{
                color: "#fff",
                textDecoration: "none",
                marginRight: "1em",
              }}
              to="/"
            >
              Home
            </Link>
            <Link
              style={{ color: "#fff", textDecoration: "none" }}
              to="/favorites"
            >
              Favorites
            </Link>
          </nav>
        </Toolbar>
      </AppBar>
      <Container maxWidth="lg">
        <ContentLayout children={children} />
      </Container>
      <Copyright />
    </>
  );
};

export default BasicLayout;
