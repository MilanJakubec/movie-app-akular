import React from "react";
import { Movie } from "./types";

interface MovieContext {
  favorites: Movie[] | [];
  addRemoveFavorite: (movie: Movie) => void;
}

const FavoritesContext = React.createContext<MovieContext>({} as MovieContext);

export const FavoritesProvider = FavoritesContext.Provider;

export default FavoritesContext;
