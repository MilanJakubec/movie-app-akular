import { Movie } from "./types";

export const insetToLocalStorage = (favorites: Movie[], movie: Movie): void => {
  const ids = favorites.map((favorite) => favorite.imdbID);

  localStorage.setItem("favorites", JSON.stringify(ids));
  localStorage.setItem("favoriteItem-" + movie.imdbID, JSON.stringify(movie));
};

export const removeFromLocalStorage = (
  favorites: Movie[],
  movie: Movie
): void => {
  const ids = favorites.map((favorite) => favorite.imdbID);

  localStorage.setItem("favorites", JSON.stringify(ids));
  localStorage.removeItem("favoriteItem-" + movie.imdbID);
};

export const getAllMovieLocalStorage = (): Movie[] | [] => {
  const storageFavorites = localStorage.getItem("favorites");

  if (storageFavorites === null) {
    return [];
  }

  const favoritesArr: string[] = JSON.parse(storageFavorites || "");

  let favorites: Movie[] = [];

  if (favoritesArr.length > 0) {
    favorites = favoritesArr.map((imdbID) =>
      JSON.parse(localStorage.getItem("favoriteItem-" + imdbID) || "")
    );
  }

  if (favorites.length > 0) {
    return favorites;
  } else {
    return [];
  }
};
